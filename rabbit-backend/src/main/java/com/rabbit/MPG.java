package com.rabbit;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.rabbit.controller.BaseController;

import java.util.Collections;

public class MPG {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/rabbit", "root", "root")
                .globalConfig(builder -> {
                    builder.author("") // 设置作者
                            .dateType(DateType.ONLY_DATE) // 用传统的java.util.Date表示日期时间类型
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("src\\main\\java"); // 指定输出目录

                })
                .packageConfig(builder -> {
                    builder.parent("com.rabbit") // 设置父包名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "src\\main\\resources\\com\\rabbit\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    String[] tables = "area,check_record,cust,dict,out_record,out_record_media,plc,pump,replenish,station,team,team_cust,team_monthly_task,user,warn,water_test_record,well"
                            .split(",");
                    builder.addInclude(tables) // 设置需要生成的表名
                            .entityBuilder()
                            .logicDeleteColumnName("del_flag") // 在实体类的逻辑删除属性上添加@TableLogic
                            .controllerBuilder()
                            .enableRestStyle() // 在Controller类头顶上加@RestController
                            .superClass(BaseController.class) // 让Controller类继承BaseController
                    ;

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
