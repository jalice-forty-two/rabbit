package com.rabbit.dto;

import java.util.Date;

public class TeamMonthlySearchDto {
    private  String name;

    private Date year;

    private  Date month;

    private  int pageNum = 1;
    private  int pageSize = 10;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    @Override
    public String toString() {
        return "TeamMonthlySearchDto{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", month=" + month +
                '}';
    }
}
