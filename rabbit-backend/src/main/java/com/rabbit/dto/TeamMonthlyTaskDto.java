package com.rabbit.dto;

import com.rabbit.entity.TeamMonthlyTask;

public class TeamMonthlyTaskDto extends TeamMonthlyTask {
    private  Long  task;

    private  Long  finish;

    public Long getTask() {
        return task;
    }

    public void setTask(Long task) {
        this.task = task;
    }

    public Long getFinish() {
        return finish;
    }

    public void setFinish(Long finish) {
        this.finish = finish;
    }
}
