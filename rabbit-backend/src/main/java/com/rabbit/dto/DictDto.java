package com.rabbit.dto;

public class DictDto {
    private  String key;
    private  String value;
    private  String keyMean;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKeyMean() {
        return keyMean;
    }

    public void setKeyMean(String keyMean) {
        this.keyMean = keyMean;
    }
}
