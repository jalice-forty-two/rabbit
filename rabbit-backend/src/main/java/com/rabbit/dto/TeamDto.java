package com.rabbit.dto;

import com.rabbit.entity.Team;

public class TeamDto extends Team {
  private  String  customer;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
}
