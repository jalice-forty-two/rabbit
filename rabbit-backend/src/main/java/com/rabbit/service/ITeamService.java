package com.rabbit.service;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamDto;
import com.rabbit.dto.TeamSearchDto;
import com.rabbit.entity.Team;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ITeamService extends IService<Team> {

    PageInfo<TeamDto> search(TeamSearchDto teamSearchDto);
}
