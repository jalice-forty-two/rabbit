package com.rabbit.service;

import com.rabbit.entity.Pump;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IPumpService extends IService<Pump> {

}
