package com.rabbit.service;

import com.rabbit.entity.OutRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 失效记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IOutRecordService extends IService<OutRecord> {

}
