package com.rabbit.service;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.DictDto;
import com.rabbit.dto.DictSearchDto;
import com.rabbit.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IDictService extends IService<Dict> {

    PageInfo<DictDto> search(DictSearchDto dicSearchDto);
}
