package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rabbit.dto.DictDto;
import com.rabbit.dto.DictSearchDto;
import com.rabbit.entity.Dict;
import com.rabbit.mapper.DictMapper;
import com.rabbit.service.IDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 数据字典 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

    @Override
    public PageInfo<DictDto> search(DictSearchDto dicSearchDto) {
        PageHelper.startPage(dicSearchDto.getPageNum(), dicSearchDto.getPageSize());
        List<DictDto> list = this.getBaseMapper().search(dicSearchDto);
        return PageInfo.of(list);
    }
}
