package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamMonthlySearchDto;
import com.rabbit.dto.TeamMonthlyTaskDto;
import com.rabbit.entity.TeamMonthlyTask;
import com.rabbit.mapper.TeamMonthlyTaskMapper;
import com.rabbit.service.ITeamMonthlyTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 队伍月度任务 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class TeamMonthlyTaskServiceImpl extends ServiceImpl<TeamMonthlyTaskMapper, TeamMonthlyTask> implements ITeamMonthlyTaskService {

    @Override
    public PageInfo<TeamMonthlyTaskDto> search(TeamMonthlySearchDto searchDto) {
        PageHelper.startPage(searchDto.getPageNum(), searchDto.getPageSize());
        List<TeamMonthlyTaskDto> list = this.getBaseMapper().search(searchDto);
        return PageInfo.of(list);

    }
}
