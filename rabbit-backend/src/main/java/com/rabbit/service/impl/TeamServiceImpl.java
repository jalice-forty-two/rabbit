package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamDto;
import com.rabbit.dto.TeamSearchDto;
import com.rabbit.entity.Team;
import com.rabbit.mapper.TeamMapper;
import com.rabbit.service.ITeamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team> implements ITeamService {

    @Override
    public PageInfo<TeamDto> search(TeamSearchDto teamSearchDto) {

        PageHelper.startPage(teamSearchDto.getPageNum(), teamSearchDto.getPageSize());
        List<TeamDto> list  = this.getBaseMapper().search(teamSearchDto);
        return  PageInfo.of(list);

    }
}
