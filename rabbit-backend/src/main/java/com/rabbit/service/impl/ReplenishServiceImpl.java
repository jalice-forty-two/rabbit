package com.rabbit.service.impl;

import com.rabbit.entity.Replenish;
import com.rabbit.mapper.ReplenishMapper;
import com.rabbit.service.IReplenishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 补液记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class ReplenishServiceImpl extends ServiceImpl<ReplenishMapper, Replenish> implements IReplenishService {

}
