package com.rabbit.service.impl;

import com.rabbit.entity.Plc;
import com.rabbit.mapper.PlcMapper;
import com.rabbit.service.IPlcService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class PlcServiceImpl extends ServiceImpl<PlcMapper, Plc> implements IPlcService {

}
