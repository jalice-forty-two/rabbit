package com.rabbit.service.impl;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.UserDto;
import com.rabbit.dto.UserSearchDto;
import com.rabbit.entity.User;
import com.rabbit.mapper.UserMapper;
import com.rabbit.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public PageInfo<UserDto> search(UserSearchDto userSearchDto) {
        List<UserDto> list = this.getBaseMapper().search(userSearchDto);
        return PageInfo.of(list);
    }
}
