package com.rabbit.service.impl;

import com.rabbit.entity.CheckRecord;
import com.rabbit.mapper.CheckRecordMapper;
import com.rabbit.service.ICheckRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class CheckRecordServiceImpl extends ServiceImpl<CheckRecordMapper, CheckRecord> implements ICheckRecordService {

}
