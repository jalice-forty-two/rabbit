package com.rabbit.service.impl;

import com.rabbit.entity.Warn;
import com.rabbit.mapper.WarnMapper;
import com.rabbit.service.IWarnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报警记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WarnServiceImpl extends ServiceImpl<WarnMapper, Warn> implements IWarnService {

}
