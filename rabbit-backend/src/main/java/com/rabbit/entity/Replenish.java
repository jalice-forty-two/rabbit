package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 补液记录
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public class Replenish implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 所属场站id
     */
    private Long stationId;

    /**
     * 队伍id
     */
    private Long teamId;

    /**
     * 补液日期
     */
    private Date replenishDate;

    /**
     * 缓蚀剂类型
     */
    private String hsjlx;

    /**
     * 缓蚀剂用量
     */
    private Double hsjyl;

    /**
     * 杀菌剂类型
     */
    private String sjjlx;

    /**
     * 杀菌剂用量
     */
    private Double sjjyl;

    /**
     * 加注类型：地面/外输/井筒
     */
    private String jzlx;

    /**
     * 泵注速度
     */
    private Double bzsd;

    /**
     * 药剂浓度
     */
    private Double yjnd;

    /**
     * 总液量（不是缓蚀剂用量+杀菌剂用量）
     */
    private Double zyl;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
    public Date getReplenishDate() {
        return replenishDate;
    }

    public void setReplenishDate(Date replenishDate) {
        this.replenishDate = replenishDate;
    }
    public String getHsjlx() {
        return hsjlx;
    }

    public void setHsjlx(String hsjlx) {
        this.hsjlx = hsjlx;
    }
    public Double getHsjyl() {
        return hsjyl;
    }

    public void setHsjyl(Double hsjyl) {
        this.hsjyl = hsjyl;
    }
    public String getSjjlx() {
        return sjjlx;
    }

    public void setSjjlx(String sjjlx) {
        this.sjjlx = sjjlx;
    }
    public Double getSjjyl() {
        return sjjyl;
    }

    public void setSjjyl(Double sjjyl) {
        this.sjjyl = sjjyl;
    }
    public String getJzlx() {
        return jzlx;
    }

    public void setJzlx(String jzlx) {
        this.jzlx = jzlx;
    }
    public Double getBzsd() {
        return bzsd;
    }

    public void setBzsd(Double bzsd) {
        this.bzsd = bzsd;
    }
    public Double getYjnd() {
        return yjnd;
    }

    public void setYjnd(Double yjnd) {
        this.yjnd = yjnd;
    }
    public Double getZyl() {
        return zyl;
    }

    public void setZyl(Double zyl) {
        this.zyl = zyl;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Replenish{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", teamId=" + teamId +
            ", replenishDate=" + replenishDate +
            ", hsjlx=" + hsjlx +
            ", hsjyl=" + hsjyl +
            ", sjjlx=" + sjjlx +
            ", sjjyl=" + sjjyl +
            ", jzlx=" + jzlx +
            ", bzsd=" + bzsd +
            ", yjnd=" + yjnd +
            ", zyl=" + zyl +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
