package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public class Well implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 所属场站id
     */
    private Long stationId;

    /**
     * FK: 所属PLC id
     */
    private Long plcId;

    /**
     * FK: 所属泵id
     */
    private Long pumpId;

    /**
     * 井名称
     */
    private String name;

    /**
     * 开关状态：0=关，1=开
     */
    private Boolean onOff;

    /**
     * 是否已安装：0=未安装，1=已安装
     */
    private Boolean installed;

    /**
     * 安装日期
     */
    private Date installDate;

    /**
     * 投产日期
     */
    private Date useDate;

    /**
     * 失效日期
     */
    private Date outDate;

    /**
     * 当前液位
     */
    private Double yeWei;

    /**
     * 工艺类型
     */
    private String gylx;

    /**
     * 最近一次补液日期
     */
    private Date zjbyrq;

    /**
     * 最近一次补液浓度
     */
    private Double zjbynd;

    /**
     * 最近一次补液类型
     */
    private String zjbylx;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public Long getPlcId() {
        return plcId;
    }

    public void setPlcId(Long plcId) {
        this.plcId = plcId;
    }
    public Long getPumpId() {
        return pumpId;
    }

    public void setPumpId(Long pumpId) {
        this.pumpId = pumpId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Boolean getOnOff() {
        return onOff;
    }

    public void setOnOff(Boolean onOff) {
        this.onOff = onOff;
    }
    public Boolean getInstalled() {
        return installed;
    }

    public void setInstalled(Boolean installed) {
        this.installed = installed;
    }
    public Date getInstallDate() {
        return installDate;
    }

    public void setInstallDate(Date installDate) {
        this.installDate = installDate;
    }
    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }
    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }
    public Double getYeWei() {
        return yeWei;
    }

    public void setYeWei(Double yeWei) {
        this.yeWei = yeWei;
    }
    public String getGylx() {
        return gylx;
    }

    public void setGylx(String gylx) {
        this.gylx = gylx;
    }
    public Date getZjbyrq() {
        return zjbyrq;
    }

    public void setZjbyrq(Date zjbyrq) {
        this.zjbyrq = zjbyrq;
    }
    public Double getZjbynd() {
        return zjbynd;
    }

    public void setZjbynd(Double zjbynd) {
        this.zjbynd = zjbynd;
    }
    public String getZjbylx() {
        return zjbylx;
    }

    public void setZjbylx(String zjbylx) {
        this.zjbylx = zjbylx;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Well{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", plcId=" + plcId +
            ", pumpId=" + pumpId +
            ", name=" + name +
            ", onOff=" + onOff +
            ", installed=" + installed +
            ", installDate=" + installDate +
            ", useDate=" + useDate +
            ", outDate=" + outDate +
            ", yeWei=" + yeWei +
            ", gylx=" + gylx +
            ", zjbyrq=" + zjbyrq +
            ", zjbynd=" + zjbynd +
            ", zjbylx=" + zjbylx +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
