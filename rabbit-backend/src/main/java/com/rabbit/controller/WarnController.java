package com.rabbit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

/**
 * <p>
 * 报警记录 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/warn")
public class WarnController extends BaseController {

}
