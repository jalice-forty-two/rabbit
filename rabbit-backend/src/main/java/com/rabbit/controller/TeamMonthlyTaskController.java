package com.rabbit.controller;


import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamDto;
import com.rabbit.dto.TeamMonthlySearchDto;
import com.rabbit.dto.TeamMonthlyTaskDto;
import com.rabbit.entity.TeamMonthlyTask;
import com.rabbit.service.ITeamMonthlyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 队伍月度任务 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/team-monthly-task")
public class TeamMonthlyTaskController extends BaseController {


    @Autowired
    ITeamMonthlyTaskService iTeamMonthlyTaskService;

    @GetMapping
    public List<TeamMonthlyTask> getAll(){
        return  this.iTeamMonthlyTaskService.list();
    }

    @GetMapping("/search")
    public PageInfo<TeamMonthlyTaskDto> search(TeamMonthlySearchDto searchDto){

      return this.iTeamMonthlyTaskService.search(searchDto);
    }
}
