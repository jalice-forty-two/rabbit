package com.rabbit.controller;


import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamDto;
import com.rabbit.dto.TeamSearchDto;
import com.rabbit.entity.Team;
import com.rabbit.service.ITeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/team")
public class TeamController extends BaseController {
    @Autowired
    ITeamService iTeamService;
    private String team;

    // 查询全部队伍

    @GetMapping("/all")
    public List<Team> getAll(){
        return this.iTeamService.list();
    }

    @GetMapping("/search")
    public PageInfo<TeamDto> search(TeamSearchDto teamSearchDto){
        return  this.iTeamService.search(teamSearchDto);


    }

}
