package com.rabbit.controller;


import com.github.pagehelper.PageInfo;
import com.rabbit.dto.DictDto;
import com.rabbit.dto.DictSearchDto;
import com.rabbit.entity.Dict;
import com.rabbit.service.IDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {
    @Autowired
    IDictService iDictService;

    @GetMapping
    public List<Dict>  getAll(){
        return  this.iDictService.list();
    }

    @GetMapping("/search")
    public PageInfo<DictDto>  search(DictSearchDto dicSearchDto){
        return  this.iDictService.search(dicSearchDto);

    }
}
