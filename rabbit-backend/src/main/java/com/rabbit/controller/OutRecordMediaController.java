package com.rabbit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

/**
 * <p>
 * 失效记录的图片和视频 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/out-record-media")
public class OutRecordMediaController extends BaseController {

}
