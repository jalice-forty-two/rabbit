package com.rabbit.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

/**
 * <p>
 * 队伍和客户的关系表 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/team-cust")
public class TeamCustController extends BaseController {

}
