package com.rabbit.controller;


import com.github.pagehelper.PageInfo;
import com.rabbit.dto.UserDto;
import com.rabbit.dto.UserSearchDto;
import com.rabbit.entity.User;
import com.rabbit.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
@Autowired
    IUserService iUserService;

        //查询全部用户
    @GetMapping

    public List<User> getAll(){
       return  this.iUserService.list();
    }
 // 根据用户名查询顾客的其他信息
    @GetMapping("/search")
    public PageInfo<UserDto> search(UserSearchDto userSearchDto){
        return  this.iUserService.search(userSearchDto);

    }
    @DeleteMapping("/{id}")
    public  boolean  remove(@PathVariable Serializable id){
        return  iUserService.removeById(id);
    }

    @GetMapping("/{id}")
    public User update(@PathVariable Serializable id){
        return  iUserService.getById(id);
    }

    @PostMapping("/save")
    public  boolean save(@RequestBody User user){
        return  iUserService.updateById(user);
    }
}
