package com.rabbit.mapper;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.UserDto;
import com.rabbit.dto.UserSearchDto;
import com.rabbit.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface UserMapper extends BaseMapper<User> {

    List<UserDto> search(UserSearchDto userSearchDto);
}
