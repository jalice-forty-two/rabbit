package com.rabbit.mapper;

import com.rabbit.entity.CheckRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface CheckRecordMapper extends BaseMapper<CheckRecord> {

}
