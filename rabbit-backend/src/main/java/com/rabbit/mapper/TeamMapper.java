package com.rabbit.mapper;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.TeamDto;
import com.rabbit.dto.TeamSearchDto;
import com.rabbit.entity.Team;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface TeamMapper extends BaseMapper<Team> {

    List<TeamDto> search(TeamSearchDto teamSearchDto);
}
