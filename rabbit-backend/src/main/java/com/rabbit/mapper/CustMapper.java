package com.rabbit.mapper;

import com.rabbit.entity.Cust;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface CustMapper extends BaseMapper<Cust> {

}
