package com.rabbit.mapper;

import com.rabbit.entity.WaterTestRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 水样检测记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface WaterTestRecordMapper extends BaseMapper<WaterTestRecord> {

}
