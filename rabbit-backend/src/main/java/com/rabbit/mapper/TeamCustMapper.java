package com.rabbit.mapper;

import com.rabbit.entity.TeamCust;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 队伍和客户的关系表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface TeamCustMapper extends BaseMapper<TeamCust> {

}
