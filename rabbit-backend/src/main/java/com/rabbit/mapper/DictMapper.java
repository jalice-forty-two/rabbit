package com.rabbit.mapper;

import com.github.pagehelper.PageInfo;
import com.rabbit.dto.DictDto;
import com.rabbit.dto.DictSearchDto;
import com.rabbit.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface DictMapper extends BaseMapper<Dict> {

    List<DictDto> search(DictSearchDto dicSearchDto);
}
