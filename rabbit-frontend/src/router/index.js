import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "../views/LoginView.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },

  {
    path: "/",
    name: "home",
    component: HomeView,
    children: [
      {
        path: "/area",
        name: "Area",
        component: () => import("../views/AreaView.vue"),
      },
      {
        path: "/checkrecord",
        name: "CheckRecord",
        component: () => import("../views/CheckRecordView.vue"),
      },
      {
        path: "/cust",
        name: "Cust",
        component: () => import("../views/CustView.vue"),
      },
      {
        path: "/dict",
        name: "Dict",
        component: () => import("../views/DictView.vue"),
      },
      {
        path: "/outrecordmedia",
        name: "OutRecordMedia",
        component: () => import("../views/OutRecordMediaView.vue"),
      },
      {
        path: "/outrecord",
        name: "OutRecord",
        component: () => import("../views/OutRecordView.vue"),
      },
      {
        path: "/plc",
        name: "Plc",
        component: () => import("../views/PlcView.vue"),
      },
      {
        path: "/pump",
        name: "Pump",
        component: () => import("../views/PumpView.vue"),
      },
      {
        path: "/replenish",
        name: "Replenish",
        component: () => import("../views/ReplenishView.vue"),
      },
      {
        path: "/station",
        name: "Station",
        component: () => import("../views/StationView.vue"),
      },
      {
        path: "/teamcust",
        name: "TeamCust",
        component: () => import("../views/TeamCustView.vue"),
      },
      {
        path: "/teammonthlytask",
        name: "TeamMonthlyTask",
        component: () => import("../views/TeamMonthlyTaskView.vue"),
      },
      {
        path: "/team",
        name: "Team",
        component: () => import("../views/TeamView.vue"),
      },
      {
        path: "/user",
        name: "User",
        component: () => import("../views/UserView.vue"),
      },
      {
        path: "/warn",
        name: "Warn",
        component: () => import("../views/WarnView.vue"),
      },
      {
        path: "/watertestrecord",
        name: "WaterTestRecord",
        component: () => import("../views/WaterTestRecordView.vue"),
      },
      {
        path: "/well",
        name: "Well",
        component: () => import("../views/WellView.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
